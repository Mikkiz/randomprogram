
package com;

import java.util.Objects;

public class Cancion {
    private String nombre;
    private String artista;
    private String album;
    public Cancion(String n,String ar, String al){
       this.nombre = n;  //NOMBRE DE LA CANCION
       this.artista = ar;   //INTERPRETE O ARTISTA
       this.album = al;    // ALBUM
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }
    
    @Override
    public String toString(){
       return  nombre+" - "+artista+  " - "+album;    
    }
    @Override
    public boolean equals(Object other){
    Cancion cancion = (Cancion) other;
    if(cancion.getArtista().equals(this.getArtista()) || cancion.getNombre().equals(this.getNombre()))
        {
            return true;
        }
    else
        {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.nombre);
        hash = 47 * hash + Objects.hashCode(this.artista);
        hash = 47 * hash + Objects.hashCode(this.album);
        return hash;
    }
}
