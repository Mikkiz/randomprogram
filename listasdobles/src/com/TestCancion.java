package com;
import java.util.*;
public class TestCancion {
    public static void main(String... args){
        Scanner input = new Scanner(System.in);
        int n=3;
        Cancion cancion[]= new Cancion[n];
        cancion[0] = new Cancion(
           "Pajarillo",
           "NAPOLEON",
           "20 Grandes Éxitos"
        );
        cancion[1] = new Cancion(
           "Ella se llamaba Martha",
           "NAPOLEON",
           "20 Grandes Éxitos"
        );
        cancion[2] = new Cancion(
           "Vive",
           "BMTH",
           "20 Grandes Éxitos"
        );
        DLLCancion canciones = new DLLCancion();
        canciones.addToHead(cancion[0]);
        canciones.addToTail(cancion[2]);
        canciones.addToHead(cancion[1]);
        //canciones.printAll();
        System.out.println(canciones.buscarCancionPorInterprete("BMTH"));
        //System.out.println(canciones.buscarCancionPorNombre("Vive"));
    }
}
