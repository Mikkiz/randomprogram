package com;

import java.io.*;
import java.util.*;
import java.util.logging.*;
/**
 *
 * @author David
 */
public class Palabra {
    public static void main(String... args){
        String linea="";
        String tmp ="";
   
        DataInputStream in = null;
        BufferedReader br = null;
        StringTokenizer st = null;
        FileInputStream fis = null;
        LinkedList leWords = null;
        LinkedList leCont = new LinkedList();
          
        try{
            fis = new FileInputStream("traba.txt");
            in = new DataInputStream(fis);
            br = new BufferedReader(new InputStreamReader(in));
            st = null;
            leWords = new LinkedList();
            int j=0;
            String leWord="";
            
            while((linea = br.readLine().trim()) != null)
            {
                st = new StringTokenizer(linea);
                j=st.countTokens();
                for(int i=0;i<j;i++)
                {
                    leWord=st.nextToken();
                    System.out.printf("Word [%d]:",i+1);
                    System.out.println(leWord);
                    if(leWords.contains(leWord)==false)
                    {
                        leWords.add(leWord);
                        leCont.add(1); 
                    }
                    else
                    {
                        int temp= (int)leCont.get(leWords.indexOf(leWord));
                        leCont.remove(leWords.indexOf(leWord));
                        leCont.add(leWords.indexOf(leWord), temp+1);
                    }
                }
                
            }      
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if(fis!=null)
            {
                try
                {
                    in.close();
                    br.close();
                    fis.close();
                }
                catch (IOException ex) 
                {
                    Logger.getLogger(Palabra.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
       File archivo = new File("res.txt");
       try 
       { 
           FileWriter escribirArchivo = new FileWriter(archivo, true);
           BufferedWriter buffer = new BufferedWriter(escribirArchivo);
           buffer.write(""+leWords.size());
           buffer.newLine();
           for(int y=0;y<leCont.size();y++)
           {
                buffer.write("Palabra: "+leWords.get(y)+" Repeticiones: "+leCont.get(y));
                buffer.newLine();
           }
           
           buffer.close(); 
       }
       catch(Exception ex)
       { 
       } 
        for(int x=0;x<leCont.size();x++)
        {
            System.out.println("Palabra: "+leWords.get(x)+" Repeticiones: "+leCont.get(x));
        }
    }
}
