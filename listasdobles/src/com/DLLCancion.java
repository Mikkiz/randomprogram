package com;
public class DLLCancion {
    NodoCancionDLL head;
    NodoCancionDLL tail;
    public DLLCancion(){
       head = tail = null;
    }
    public boolean isEmpty(){
       return head == null;
    }
    //METODO PARA AGREGAR UN NODO AL INICIO DE LA
    //ESTRUCTURA DEL TIPO LISTA DOBLEMENTE ENLAZADA
    public void addToHead(Cancion el){
        if(!isEmpty()){
           head = new NodoCancionDLL(el,null,head);
           head.next.prev = head;
        }else{
           head = tail = new NodoCancionDLL(el);
        }
    }
    
    //METODO PARA AGREGAR UN NODO AL FINAL DE LA
    //ESTRUCTURA DEL TIPO LISTA DOBLEMENTE ENLAZADA
    public void addToTail(Cancion el){
       if(!isEmpty()){
           tail = new NodoCancionDLL(el,tail,null);
           tail.prev.next = tail;
        }else{
           head = tail = new NodoCancionDLL(el);
        } 
    }
    
    public void printAll(){
        NodoCancionDLL tmp;
        for(tmp = head; tmp !=null; tmp = tmp.next){
           System.out.print(tmp.info+" - ");   
           System.out.println();
        }
    }        
    
    public String buscarCancionPorNombre(String nombre)
    {
        int flag = 0; String leResult = "Busqueda....";
        NodoCancionDLL tmp;
        for(tmp = head; tmp !=null ; tmp = tmp.next)
        {
            if(tmp.info.getNombre().equals(nombre))
            {
                flag++;
                leResult = leResult+"\n\n" + tmp.info.toString();
                System.out.println(tmp.info.toString());
            }
        }
        if(flag == 0)
        {
            leResult = "\n No se encontro cancion con ese nombre.";
        }
        return leResult+"\n\n FINALIZADO!";
    }
    public String buscarCancionPorInterprete(String artista)
    {
        int flag = 0; String leResult = "Busqueda....";
        NodoCancionDLL tmp;
        for(tmp = head; tmp !=null ; tmp = tmp.next)
        {
            if(tmp.info.getArtista().equals(artista))
            {
                flag++;
                leResult = leResult+"\n\n" + tmp.info.toString();
                System.out.println(tmp.info.toString());
            }
        }
        if(flag == 0)
        {
            leResult = "\n El interprete no se encontro.";
        }
        return leResult+"\n\n FINALIZADO!";
    }
}