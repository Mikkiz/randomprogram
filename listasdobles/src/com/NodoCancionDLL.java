package com;
public class NodoCancionDLL {
    Cancion info;
    NodoCancionDLL next;
    NodoCancionDLL prev;
    public NodoCancionDLL(Cancion e,NodoCancionDLL p,NodoCancionDLL n){
       this.info = e;
       this.prev = p;
       this.next = n;
    }    
    public NodoCancionDLL(Cancion el){
       this(el,null,null);
    }            
}
